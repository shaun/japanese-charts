# Charts to help me learn Japanese


## Verb Types

```mermaid
graph LR
	start(Start)
    suru_kuru(Is it suru or kuru?)
    eru_iru(Does it end it eru or iru?)
    type1((Type 1 Verb))
    type2((Type 2 Verb))
    type3((Type 3 Verb))

    start --> suru_kuru
    
    suru_kuru --Yes--> type3
    suru_kuru --No--> eru_iru
    
    eru_iru --Yes--> type2
    eru_iru --No--> type1
```

## Type 1 Verb Conjugation
```mermaid
graph LR
  a_line(あ-line)
  i_line(い-line)
  u_line(う-line)
  e_line(え-line)
  o_line(お-line)

  negative_form("+nai (negative form)")
  polite_form("+masu (polite form)")
  dictionary_form(dictionary form)
  capable_form("+ru (capable form)")
  lets_form("+u (let's form)")

  a_line --> negative_form
  i_line --> polite_form
  u_line --> dictionary_form
  e_line --> capable_form
  o_line --> lets_form
  ```
##### Type 1 Verb Example - 飲む (nomu)
  
  ```mermaid
  graph LR
  subgraph example [" "]
    no((飲))
    ma(ま)
    mi(み)
    mu(む)
    me(め)
    mo(も)
    nai(ない)
    masu(ます)
    ru(る)
    u(う)
  
    no --> ma --> nai
    no --> mi --> masu
    no --> mu
    no --> me　--> ru
    no --> mo --> u
  end
  ```